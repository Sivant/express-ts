/**
 * Created Date: Wednesday, November 30th 2022, 10:48:26 pm
 * Author: CodingGorit
 * -----
 * Last Modified: Sat Mar 18 2023
 * Modified By: CodingGorit
 * -----
 * Copyright (c) 2022 fmin-courses
 * ------------------------------------
 * Javascript will save your soul!
 */

import dayjs from "dayjs";
import { Result } from '../common/Result';
import MailManager, { IMailMessage, MailMessageCodeEnum } from "../manager/MailManager";
import { ResultCodeEnum, ResultMessageEnum } from "../enums/ResultEmums";
import log from "../utils/log";

const TAG = "MailService";
const database = "t_mail_records"

export const sendCustomMessage = (message: object): Promise<Result<any>> => {
    return new Promise((resolve, reject) => {
        const res = message as IMailMessage;
        MailManager.getInstance().sendCustomMail(res, (err: MailMessageCodeEnum, reason: string, response?: any) => {
            log.info(`${TAG} sendCustomMessage, errCode is => ${err}, reason is => ${reason}`);
            // 数据库操作 TODO
            if (err === MailMessageCodeEnum.FAIL) {
                reject(new Result(
                    ResultCodeEnum.NOTIFY_FAILED,
                    reason,
                    null
                ));
            } else if (err === MailMessageCodeEnum.SUCCESS) {
                let data = null;
                if (response) {
                    data = response;
                }
                resolve(new Result(
                    ResultCodeEnum.NOTIFY_SUCCESS,
                    reason,
                    data
                ));
            } else if (err === MailMessageCodeEnum.ERROR) {
                reject(Result.error(reason));
            }
        })
    });
}