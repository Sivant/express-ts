/**
 * Created Date: Thursday, October 20th 2022, 1:16:23 am
 * Author: CodingGorit
 * -----
 * Last Modified: Fri Oct 28 2022
 * Modified By: CodingGorit
 * -----
 * Copyright (c) 2022 fmin-courses
 * ------------------------------------
 * Javascript will save your soul!
 */

import { IUser } from "../models";
import query from "../utils/DBUtils";

// 查询用户 test，由于数据库资源丢失，此数据不可查询
export const getUser = async () => {
    const sql = 'select * from bm_user';

    // 异步改成同步
    const result = await query(sql);

    return result as Array<IUser>;
}