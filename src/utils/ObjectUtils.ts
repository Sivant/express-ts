/**
 * Created Date: Wednesday, November 30th 2022, 1:15:01 am
 * Author: CodingGorit
 * -----
 * Last Modified: Wed Nov 30 2022
 * Modified By: CodingGorit
 * -----
 * Copyright (c) 2022 fmin-courses
 * ------------------------------------
 * Javascript will save your soul!
 */
const toString = Object.prototype.toString;

export function isPlainObject(val: any): val is Object {
    return toString.call(val) === '[object Object]';
}

export function deepMerge(...objs: any[]): any {
    const result = Object.create(null);

    objs.forEach(obj => {
        if (obj) {
            Object.keys(obj).forEach(key => {
                const val = obj[key];
                if (isPlainObject(val)) {
                    if (isPlainObject(result[key])) {
                        result[key] = deepMerge(result[key], val);
                    } else {
                        result[key] = deepMerge(val);
                    }
                } else {
                    result[key] = val;
                }
            })
        }
    })

    return result;
}