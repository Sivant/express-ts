/**
 * Created Date: Saturday, March 18th 2023, 12:03:58 am
 * Author: CodingGorit
 * -----
 * Last Modified: Sat Mar 18 2023
 * Modified By: CodingGorit
 * -----
 * Copyright © 2019 —— 2023 fmin-courses All Rights Reserved
 * ------------------------------------
 * Javascript will save your soul!
 */


const TAG = "ValidatorUtils";

class ValidatorUtils {
    private static _instance: ValidatorUtils | null;
    private static regxp = {
        mail: /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
    }

    static getInstance() {
        if (!this._instance) {
            this._instance = new this();
        }
        return this._instance;
    }

    static releaseInstance() {
        if (this._instance) {
            this._instance = null;
        }
    }

    static isValidMaill (mail: string) {
        if (typeof mail !== 'string') {
            throw new Error("mail is not a string !");
        }
        if (!mail || mail.length === 0) {
            return false;
        }
        return ValidatorUtils.regxp.mail.test(mail);
    }
}

export = ValidatorUtils;