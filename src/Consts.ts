/**
 * Created Date: Saturday, November 5th 2022, 11:22:01 pm
 * Author: CodingGorit
 * -----
 * Last Modified: Sun Nov 06 2022
 * Modified By: CodingGorit
 * -----
 * Copyright (c) 2022 fmin-courses
 * ------------------------------------
 * Javascript will save your soul!
 */


export const Consts = {

    // 网络连接错误
    ERR_INTERNET_DISCONNECTED: "err_internet_disconnected"
}