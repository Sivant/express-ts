/**
 * Created Date: Saturday, November 5th 2022, 10:46:51 pm
 * Author: CodingGorit
 * -----
 * Last Modified: Tue Nov 08 2022
 * Modified By: CodingGorit
 * -----
 * Copyright (c) 2022 fmin-courses
 * ------------------------------------
 * Javascript will save your soul!
 */

// 接口限制配置

import rateLimit from 'express-rate-limit';
import { ResultCodeEnum, ResultMessageEnum } from '../enums/ResultEmums';

const response = {
    code: ResultCodeEnum.REPEAT_REQUEST,
    msg: ResultMessageEnum.REPEAT_REQUEST
};

// 获取随机验证码最大为 1分钟 15 次
export const getVerifyCodeLimit = rateLimit({
    windowMs: 1 * 60 * 1000, // 1 minute
	max: 15, // Limit each IP to 100 requests per `window` (here, per 15 minutes)
	standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
	legacyHeaders: false, // Disable the `X-RateLimit-*` headers
    message: response
});

// 邮件通知消接口限制请求次数
export const mailApiLimiter = rateLimit({
	windowMs: 15 * 60 * 1000, // 15 minute
	max: 1, // Limit each IP to 100 requests per `window` (here, per 15 minutes)
	standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
	legacyHeaders: false, // Disable the `X-RateLimit-*` headers
    message: response
});
