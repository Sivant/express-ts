/**
 * Created Date: Thursday, October 20th 2022, 1:47:32 am
 * Author: CodingGorit
 * -----
 * Last Modified: Fri Oct 28 2022
 * Modified By: CodingGorit
 * -----
 * Copyright (c) 2022 fmin-courses
 * ------------------------------------
 * Javascript will save your soul!
 */


// count_down / count_down_data

export interface IUser {
    id: number;
    username: string;
    password: string;
}

export interface IStudent {
    stuId?: string;  // 10  ? 表示可选参数
    stuName: string; // 20
    stuAge: number; // 3
    classz: string; // 30
}
