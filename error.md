

# 问题记录

> 此为 记录

## 1. node 连接 MySQL 问题记录

- 默认只能连接 mysql5.7， mysql8 由于插件缘故，导致密码加密方式变化，需要将 mysql8 的密码设置为 native_password 即可, 如下

```sql

ALTER USER ‘root’@‘localhost’ IDENTIFIED WITH mysql_native_password BY ‘password’; # 更改新的密码，可以将密码设置为简单类型

FLUSH PRIVILEGES;

```